import React, { PropTypes, Component } from 'react'
import App from '../containers/App'

export default class DataTable extends Component {

  formatDate(date){
    return date.split(' ')[0];
  }
  render() {
    return (
      <table>
        <tbody>
        <tr className="Header">
          <td>Title</td>
          <td>Status</td>
          <td>Updated</td>
          <td>Created</td>
          <td>Delete</td>
        </tr>
        {this.props.model.map((post, i) =>
          <tr key={post.id} className={post.status}>
            <td>{post.title}</td>
            <td>{post.status}</td>
            <td>{this.formatDate(post.updated_at)}</td>
            <td>{this.formatDate(post.created_at)}</td>
            <td className="link" onClick={this.props.onClick.bind(this,post.id)}>Delete</td>
          </tr>
        )}
        </tbody>
      </table>
    )
  }
}

DataTable.propTypes = {
  model: PropTypes.array.isRequired
}
