import React, { Component, PropTypes } from 'react'

export default class StatusSelector extends Component {
  render() {
    const { value, onChange, options } = this.props

    return (
      <div className="headerContainer">
        <h1>Requests</h1>
        <div className="statusContainer">
          <div className="Header">Status</div>
          <select onChange={e => onChange(e.target.value)}
                  value={value}>
            {options.map(option =>
              <option value={option} key={option}>
                {option}
              </option>)
            }
          </select>
        </div>
      </div>
    )
  }
}

StatusSelector.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.string.isRequired
  ).isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}
