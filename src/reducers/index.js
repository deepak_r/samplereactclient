import { combineReducers } from 'redux'
import { SELECT_VALUE, RECEIVE_POSTS } from '../actions'

function selectorValue(state = 'All', action) {
  switch (action.type) {
  case SELECT_VALUE:
    return action.value
  default:
    return state
  }
}

function posts(state = {items: [] }, action) {
  switch (action.type) {
    case RECEIVE_POSTS:
      return Object.assign({}, state, { items: action.posts })
    default:
      return state
  }
}

function postsByValue(state = {}, action) {
  switch (action.type) {
    case RECEIVE_POSTS:
      return Object.assign({}, state, {
        [action.value]: posts(state[action.value], action)
      })
    default:
      return state
  }
}

const rootReducer = combineReducers({postsByValue,selectorValue})
export default rootReducer
