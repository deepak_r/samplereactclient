import 'babel-polyfill'

import React from 'react'
import { render } from 'react-dom'
import Root from './containers/Root'
import './style/index.css';

render(
  <Root />,
  document.getElementById('root')
)
