import fetch from 'isomorphic-fetch'

export const RECEIVE_POSTS = 'RECEIVE_POSTS'
export const SELECT_VALUE = 'SELECT_VALUE'

export function setSelectorValue(value) {
  return {
    type: SELECT_VALUE,
    value
  }
}

function receivePosts(value, json) {
  return {
    type: RECEIVE_POSTS,
    value,
    posts: json.map(child => child)
  }
}

export function fetchModelData(value) {
  return (dispatch, getState) => {
    return fetch(`http://localhost:4730/getData?status=${value}`)
      .then(response => response.json())
      .then(json => dispatch(receivePosts(value, json)))
  }
}

export function deleteModelData(id, status){
  return (dispatch, getState) => {
    return fetch(`http://localhost:4730/deleteData?id=${id}`)
      .then(response => response.json())
      .then(json => dispatch(fetchModelData(status)))
  }
}
