import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { setSelectorValue, fetchModelData, deleteModelData} from '../actions'
import StatusSelector from '../components/StatusSelector'
import DataTable from '../components/DataTable'

class App extends Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.deleteModelData = this.deleteModelData.bind(this)
  }

  componentDidMount() {
    const { dispatch, selectorValue } = this.props
    dispatch(fetchModelData(selectorValue))
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectorValue !== this.props.selectorValue) {
      const { dispatch, selectorValue } = nextProps
      dispatch(fetchModelData(selectorValue))
    }
  }

  handleChange(nextValue) {
    this.props.dispatch(setSelectorValue(nextValue))
  }

  deleteModelData(item){
    this.props.dispatch(deleteModelData(item,this.props.selectorValue))
  }
  render() {
    const { selectorValue, model} = this.props
    return (
      <div>
        <div>
          <StatusSelector value={selectorValue}
                  onChange={this.handleChange}
                  options={[ 'All', 'Approved', 'Pending', 'Denied']} />
        </div>
        <div className="mainContainer">
          {model.length === 0 &&
            <h2>No Record found.</h2>
          }
          {model.length > 0 &&
            <div>
              <DataTable model={model} onClick={this.deleteModelData.bind()}/>
            </div>
          }
        </div>
      </div>
    )
  }
}

App.propTypes = {
  selectorValue: PropTypes.string.isRequired,
  model: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const { selectorValue, postsByValue } = state
  const { items: model } = postsByValue[selectorValue] || { items: [] }

  return {
    selectorValue,
    model
  }
}

export default connect(mapStateToProps)(App)
